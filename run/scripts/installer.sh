#!/bin/bash

module=$1

echo $module

echo "Installing module $module..."
basedir="/opt/magic_mirror"
installer="$basedir/module_installer/$module"
moduledir="$basedir/modules/$module"
module_backup_dir="$basedir/mount_ori/modules"
mkdir -p $moduledir
cd $moduledir
$installer
echo "Moving module to backup dir"
mv -u $moduledir $module_backup_dir
echo "Installed module $module..."
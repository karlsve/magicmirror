#!/bin/sh

base_dir="/opt/magic_mirror"
module_dir="${base_dir}/modules"
css_dir="${base_dir}/css"
config_dir="${base_dir}/config"

mv -u /opt/magic_mirror/mount_ori/modules/* $module_dir
mv -u /opt/magic_mirror/mount_ori/css/* $css_dir
mv -u /opt/magic_mirror/mount_ori/config/* $config_dir

ls $base_dir

[ ! -f ${css_dir}/custom.css ] && touch ${css_dir}/custom.css

if [ "$MM_SHOW_CURSOR" = "true" ]; then 
  echo "enable mouse cursor ..."
  sed -i "s|  cursor: .*;|  cursor: auto;|" /opt/magic_mirror/css/main.css
fi

if [ "$StartEnv" = "test" ]; then
  echo "start tests ..."
  set -e

  Xvfb :99 -screen 0 1024x768x16 &
  export DISPLAY=:99

  # adjust test timeouts
  sed -i "s:test.timeout(10000):test.timeout(30000):g" tests/e2e/global-setup.js
  cat tests/e2e/global-setup.js

  if [ "${CI_COMMIT_REF_NAME}" = "master" ]; then
    grunt
  else
    echo "/mount_ori/**/*" >> .prettierignore
    npm run test:prettier
    npm run test:js
    npm run test:css
  fi;
  npm run test:e2e
  npm run test:unit
else
  echo "start magicmirror"

  exec "$@"
fi

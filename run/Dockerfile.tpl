FROM karsten13/magicmirror:latest

USER root
RUN apt-get update 
RUN apt-get install -y expect alsa-utils wget libasound2-dev sox libsox-fmt-all libsox-fmt-mp3 gcc-7 build-essential mpg321 libmagic-dev libatlas-base-dev curl python3
RUN usermod -a -G audio node
COPY scripts/installer.sh /opt/magic_mirror/
RUN chmod +x /opt/magic_mirror/installer.sh
RUN chown node:node /opt/magic_mirror/installer.sh
COPY scripts/entrypoint.sh /opt/magic_mirror/
RUN chmod +x /opt/magic_mirror/entrypoint.sh
RUN chown node:node /opt/magic_mirror/entrypoint.sh

USER node
WORKDIR /opt/magic_mirror
RUN mkdir /opt/magic_mirror/module_installer
